// "C:\Program Files\MongoDB 2.6 Standard\bin\mongod.exe" --dbpath C:\Users\Robert\Documents\DBs\WM2014

var Factory = function (Schema, mongoose) {

    this.Schema = Schema;
    this.mongoose = mongoose;

    this.createSchemas = function () {
        UserSchema = new this.Schema({
            email: String,
            name: String,
            password: String,
            coins: Number,
            isAdmin: Boolean,
            created: { type: Date, default: Date.now }
        });
        this.User = mongoose.model('User', UserSchema);

        TeamSchema = new this.Schema({
            country: String
        });
        this.Team = mongoose.model('Team', TeamSchema);

        GameSchema = new this.Schema({
            teamA: {type: Schema.ObjectId, ref: 'Team'},
            teamB: { type: Schema.ObjectId, ref: 'Team' },
            initRatioTeamA: Number,
            initRatioTeamB: Number,
            initRatioTie: Number,
            finalRatioTeamA: Number,
            finalRatioTeamB: Number,
            finalRatioTie: Number,
            date: Date,
            tie: Boolean,
            winner: { type: Schema.ObjectId, ref: 'Team' }
        });
        this.Game = mongoose.model('Game', GameSchema);

        BetSchema = new this.Schema({
            game: { type: Schema.ObjectId, ref: 'Game' },
            user: { type: Schema.ObjectId, ref: 'User' },
            tie: Boolean,
            winner: { type: Schema.ObjectId, ref: 'Team' },
            date: Date,
            coins: Number,
            profit: Number
        });
        this.Bet = mongoose.model('Bet', BetSchema);
    }

    this.getUser = function (query, res) {
        this.User.find(query, function (error, output) {
            if (error) {
                console.log(error);
            } else {
                res.json(output);
            }
        });
    }

    this.getTeam = function (query, res) {
        this.Team.find(query, function (error, output) {
            if (error) {
                console.log(error);
            } else {
                res.json(output);
            }
        });
    }

    this.getGame = function (query, res) {
        this.Game.find(query, function (error, output) {
            if (error) {
                console.log(error);
            } else {
                res.json(output);
            }
        });
    }

    this.getBet = function (query, res) {
        this.Bet.find(query, function (error, output) {
            if (error) {
                console.log(error);
            } else {
                res.json(output);
            }
        });
    }
}

module.exports = Factory;