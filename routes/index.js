var express = require('express');
var router = express.Router();

router.get('/', function (req, res) {
    res.render('index',
        {   title: 'Football Wetten',
            description: 'Startseite von Footballwetten',
            keywords: 'football, nfl, bet, wetten' });
});

module.exports = router;