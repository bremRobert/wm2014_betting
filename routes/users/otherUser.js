var express = require('express');
var router = express.Router();
var moment = require('moment');
var momentTz = require('moment-timezone');
var numeral = require('numeral');

var userObject = require('../../daos/User.js');
var betObject = require('../../daos/Bet.js');

router.get('/otherUser/:id', function (req, res) {
    var id = req.param('id');
    displayBetsByUser(id, res, req, undefined);
});

function displayBetsByUser(id, res, req, errorText) {
    userObject.User.getAll(res)(function (games) {
        betObject.Bet.findByUserId(res, id)(function (bets) {

            var correctBets = 0;
            var wrongBets = 0;
            for (var index = 0; index < bets.length; index++) {
                var bet = bets[index];
                if (bet.profit) {
                    if (bet.profit > 0) {
                        correctBets++;
                    } else if (bet.profit < 0) {
                        wrongBets++;
                    }
                }
            }

            var user = undefined;
            if (bets && bets[0]) {
                user = bets[0].user;
            }

            var now = momentTz.tz(Date.now(), "Europe/Zurich");
            res.render('users/otherUser', {bets: bets, games: games, moment: moment,
                now: now, user: user, errors: errorText, correctBets: correctBets,
                wrongBets: wrongBets, isAdmin: req.session.isAdmin, numeral: numeral});
        });
    });
};

module.exports = router;