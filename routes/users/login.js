var express = require('express');
var router = express.Router();
var passwordHash = require('password-hash');

var userObject = require('../../daos/User.js');

router.get('/login', function (req, res) {
    res.render('users/login', { invalid: false})
});

router.post('/login', function (req, res) {
    var email = req.param('email');
    var pass = req.param('pass');

    if (!(email && pass)) {
        return  res.render('users/login', { invalid: true});
    }

    userObject.User.findByEmail(res, email)(function (user) {
        if (!user) {
            return  res.render('users/signup', {invalid: true });
        }
        if (!passwordHash.verify(req.param('pass'), user.password)) {
            return  res.render('users/login', {invalid: true });
        }

        req.session.isLoggedIn = true;
        req.session.isAdmin = user.isAdmin;
        req.session.user = email;
        req.session.userId = user._id;

        res.redirect('/user');
    });
});

router.get('/logout', function (req, res) {
    req.session.isLoggedIn = false;
    req.session.user = undefined;
    req.session.isAdmin = false;
    req.session.userId = undefined;

    res.render('users/login', { invalid: false})
});

router.get('/signup', function (req, res) {
    res.render('users/signup', {exists: false, invalid: false})
});

router.post('/signup', function (req, res) {
    var email = req.param('email');
    var pass = req.param('pass');

    if (!(email && pass)) {
        return  res.render('users/signup', {exists: false, invalid: true});
    }

    userObject.User.findByEmail(res, email)(function (foundUser) {
        if (foundUser) {
            return  res.render('users/signup', {exists: true, invalid: false});
        }

        var hashedPassword = passwordHash.generate(pass);

        var user = res.factory.User();
        user.email = email;
        user.password = hashedPassword;
        user.coins = 100;

        user.save(function (err) {
            if (err) {
                console.log(err);
            } else {

                req.session.isLoggedIn = true;
                req.session.user = email;
                req.session.isAdmin = false;
                req.session.userId = user._id;

                res.redirect('/user');
            }
        });
    });
});

module.exports = router;