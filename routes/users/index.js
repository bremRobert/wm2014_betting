var express = require('express');
var router = express.Router();
var passwordHash = require('password-hash');

var userObject = require('../../daos/User.js');

router.get('/', function (req, res) {
    if (!req.session.isLoggedIn) {
        res.render('users/login');
    }

    userObject.User.findByEmail(res, req.session.user)(function (user) {
        res.render('users/user', {user: user, isAdmin: req.session.isAdmin});
    });
});

router.post('/', function (req, res, next) {
    if (!req.session.isLoggedIn) {
        res.render('users/login');
    }

    var name = req.param('name');
    var email = req.param('email');

    userObject.User.findByEmail(res, req.session.user)(function (user) {
        user.name = name;
        user.email = email;
        user.save();

        res.render('users/user', {user: user, isAdmin: req.session.isAdmin});
    });
});

router.get('/changePassword', function (req, res) {
    if (!req.session.isLoggedIn) {
        res.render('users/login');
    }

    userObject.User.findByEmail(res, req.session.user)(function (user) {
        res.render('users/changePassword', {user: user, isAdmin: req.session.isAdmin});
    });
});

router.post('/changePassword', function (req, res, next) {
    if (!req.session.isLoggedIn) {
        res.render('users/login');
    }

    var pass = req.param('pass');

    userObject.User.findByEmail(res, req.session.user)(function (user) {
        var hashedPassword = passwordHash.generate(pass);
        user.password = hashedPassword;
        user.save();

        res.render('users/user', {user: user, isAdmin: req.session.isAdmin});
    });
});

module.exports = router;