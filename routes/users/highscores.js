var express = require('express');
var router = express.Router();
var momentTz = require('moment-timezone');

var userObject = require('../../daos/User.js');
var betObject = require('../../daos/Bet.js');

router.get('/highscores', function (req, res) {
    displayAllUsers(res, req);
});

function displayAllUsers(res, req) {
    userObject.User.getAll(res)(function (users) {
        getLockedCoins(0, users, res, req);
    });
};

function getLockedCoins(i, users, res, req) {
    if (i < users.length) {
        var user = users[i];
        betObject.Bet.findByUserId(res, user._id)(function (bets) {
            var lockedCoins = 0;

            for (var index = 0; index < bets.length; ++index) {
                var bet = bets[index];
                if (bet.game && (bet.game.date > momentTz.tz(Date.now(), "Europe/Zurich"))) {
                    lockedCoins += bet.coins;
                }
            }

            user.coins += lockedCoins;
            getLockedCoins(i + 1, users, res, req);
        });
    } else {
        users.sort(function (a, b) {
            if (a.coins < b.coins) {
                return 1;
            }
            if (a.coins > b.coins) {
                return -1;
            }
            return 0;
        });

        res.render('users/highscores', {users: users, isAdmin: req.session.isAdmin});
    }
}

module.exports = router;