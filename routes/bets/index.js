var express = require('express');
var router = express.Router();
var moment = require('moment');
var momentTz = require('moment-timezone');
var numeral = require('numeral');

var ratioCalculator = require('../../controller/RatioCalculator.js');
var userObject = require('../../daos/User.js');
var betObject = require('../../daos/Bet.js');
var gameObject = require('../../daos/Game.js');

var betsPerPage = {};

router.get('/:pageNumber', function (req, res) {
    var reload = req.param("refresh") === "true";
    displayBetsByLogedinUser(res, req, reload);
});

router.post('/:pageNumber', function (req, res) {
    var game = req.param('game');
    var userMail = req.session.user;
    var tie = req.param('tie');
    var winner = req.param('winner');
    var coins = req.param('coins');

    if (tie === 'true') {
        winner = undefined;
    }

    userObject.User.findByEmail(res, userMail)(function (user) {
        if (parseInt(coins) > user.coins) {
            displayBetsByLogedinUser(res, req, false, 'Sie haben zuwenig Punkte.');
            return;
        }

        saveBetWithUser(res, game, user, tie, winner, coins, req);
    });
});

function saveBetWithUser(res, game, user, tie, winner, coins, req) {
    var date = momentTz.tz(Date.now(), "Europe/Zurich");
    var newBet = new res.factory.Bet({
            game: game,
            user: user._id,
            tie: tie == 'true',
            winner: winner,
            coins: parseInt(coins),
            date: date
        }
    );
    newBet.save(function (err) {
        if (err) {
            console.log(err);
        } else {
            user.coins -= parseInt(coins);
            user.save(function (userErr) {
                if (userErr) {
                    console.log(userErr);
                } else {
                    displayBetsByLogedinUser(res, req, true, undefined);
                }
            })
        }
    });
}

router.get('/getOptionsForGame/:id', function (req, res) {
    gameObject.Game.findByUserId(res, req.params.id)(function (game) {
        res.render('bets/optionsForGame', {game: game});
    });
});

function displayBetsByLogedinUser(res, req, reloadFromDB, errorText) {
    var userBetsPerPage;
    if (req.session.userId in betsPerPage) {
        userBetsPerPage = betsPerPage[req.session.userId];
    } else {
        betsPerPage[req.session.userId] = new betObject.Bet();
        userBetsPerPage = betsPerPage[req.session.userId];
    }
    userBetsPerPage.getResolvedBetsByUserPage(req.params.pageNumber, reloadFromDB, res, req.session.userId)(function (resolvedBetsForPageDisplay) {
        gameObject.Game.getAll(res)(function (games) {
            var now = momentTz.tz(Date.now(), "Europe/Zurich");
            res.render('bets/bets', {bets: resolvedBetsForPageDisplay, games: games, moment: moment,
                now: now, isAdmin: req.session.isAdmin, errors: errorText, numeral: numeral,
                pageNumber: req.params.pageNumber, pages: userBetsPerPage.getPageSize()});
        });
    });
};

router.get('/deleteBet/:id', function (req, res) {
    if (!req.session.isLoggedIn) {
        res.render('users/login');
    }

    req.params.pageNumber = 1;
    userObject.User.findByEmail(res, req.session.user)(function (user) {
        betObject.Bet.getById(res, req.params.id)(function (bet) {
            var game = bet.game;
            if (game.date < bet.date) {
                displayBetsByLogedinUser(res, req, false);
                return;
            }

            user.coins += bet.coins;
            user.save(function (saveErr) {
                if (saveErr) {
                    console.log(saveErr);
                } else {
                    res.factory.Bet.remove({_id: req.params.id}, function (err) {
                        if (err) {
                            console.log(err);
                        } else {
                            displayBetsByLogedinUser(res, req, true);
                        }
                    });
                }
            });
        });
    });
});

module.exports = router;