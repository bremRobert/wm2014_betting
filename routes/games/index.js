var express = require('express');
var router = express.Router();
var moment = require('moment');
var momentTz = require('moment-timezone');
var numeral = require('numeral');

var teamObject = require('../../daos/Team.js');
var gameObject = require('../../daos/Game.js');
var betObject = require('../../daos/Bet.js');
var ratioCalculator = require('../../controller/RatioCalculator.js');


router.get('/', function (req, res) {
    displayAllGames(res, req);
});

router.get('/onlyOpenGames', function (req, res) {
    displayAllGames(res, req, 'onlyOpenGames');
});

router.get('/allGames', function (req, res) {
    displayAllGames(res, req, 'allGames');
});

router.post('/', function (req, res) {
    var team1 = req.param('team1');
    var team2 = req.param('team2');
    var ratioTeam1 = req.param('ratioTeamA');
    var ratioTeam2 = req.param('ratioTeamB');
    var ratioTie = req.param('ratioTie');
    var year = req.param('year');
    var month = req.param('month');
    var day = req.param('day');
    var hour = req.param('hour');
    var minute = req.param('minute');

    var dateString = parseInt(year) + "-" + parseInt(month) + "-" + parseInt(day) + " " + parseInt(hour) + ":" + parseInt(minute);
//     var date = momentTz.tz(dateString, "Europe/Zurich");
    var date = new Date(dateString);

    var newGame = new res.factory.Game({
            teamA: team1,
            teamB: team2,
            initRatioTeamA: ratioTeam1,
            initRatioTeamB: ratioTeam2,
            initRatioTie: ratioTie,
            date: date
        }
    );
    newGame.save(function (err) {
        if (err) {
            console.log(err);
        } else {
            displayAllGames(res, req);
        }
    });
});

function displayAllGames(res, req, jadeRef) {
    teamObject.Team.getAll(res)(function (teams) {
        gameObject.Game.getAll(res)(function (games) {
            betObject.Bet.getAll(res)(function (bets) {
                var gamesWithRatios = new ratioCalculator.RatioCalculator().getGamesWithRatio(games, bets);
                if (jadeRef) {
                    res.render('games/' + jadeRef, {games: gamesWithRatios, teams: teams, moment: moment, isAdmin: req.session.isAdmin, numeral: numeral});
                } else {
                    res.render('games/games', {games: gamesWithRatios, teams: teams, moment: moment, isAdmin: req.session.isAdmin, numeral: numeral});
                }
            });
        });
    });
}

router.get('/deleteGame/:id', function (req, res) {
    if (!req.session.isAdmin) {
        res.render('users/login');
    }

    res.factory.Game.remove({_id: req.params.id}, function (err) {
        if (err) {
            console.log(err);
        } else {
            displayAllGames(res, req);
        }
    })
});

module.exports = router;