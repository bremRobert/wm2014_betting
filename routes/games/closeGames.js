var express = require('express');
var router = express.Router();
var moment = require('moment');

var ratioCalculator = require('../../controller/RatioCalculator.js');
var gameObject = require('../../daos/Game.js');
var betObject = require('../../daos/Bet.js');
var userObject = require('../../daos/User.js');

router.get('/closeGames', function (req, res) {
    displayAllGames(res, req);
});

function displayAllGames(res, req) {
    gameObject.Game.getAll(res)(function (games) {
        res.render('games/closeGames', {games: games, moment: moment, isAdmin: req.session.isAdmin});
    });
}

router.post('/closeGames', function (req, res) {
    gameObject.Game.getAll(res)(function (games) {
        setTieOrWinner(0, games, res, req);
    });
});

function setTieOrWinner(index, games, res, req) {
    if (index < games.length) {
        var gameResult = req.param('result' + games[index]._id);
        if (gameResult === 'tie') {
            tieGame(res, games[index]._id)(function (callback) {
                    setTieOrWinner(index + 1, games, res, req);
                },
                function (error) {
                    console.log(error);
                });
        } else if (gameResult === 'open' || !gameResult) {
            setTieOrWinner(index + 1, games, res, req);
        } else { // WinnerID is set
            setGameWinner(res, games[index], gameResult)(function (callback) {
                    setTieOrWinner(index + 1, games, res, req);
                }, function (error) {
                    console.log(error);
                }
            );
        }
    } else {
        displayAllGames(res, req);
    }
}

function updateBetsOnTie(res, game) {
    return function (callback, errorback) {
        betObject.Bet.findByGameId(res, game._id)(function (bets) {
            var i = 0;
            updateUserTie(i, bets, bets[i], new ratioCalculator.RatioCalculator().getTieRatio(bets, game), res)(
                function (callbackUserTie) {
                    callback();
                },
                function (errorUserTie) {
                    errorback(errorUserTie);
                }
            );
        });
    }
}

function updateUserTieLoop(i, bets, currentBet, tieRatio, res, callback, errorback) {
    if (i < bets.length) {
        userObject.User.findById(res, currentBet.user)(function (user) {
            if (currentBet.tie) {
                var profit = Math.ceil(currentBet.coins * tieRatio);
                user.coins += profit;
                user.save(function (err) {
                    if (err) {
                        errorback(err);
                    } else {
                        currentBet.profit = profit - currentBet.coins;
                        currentBet.save(function (err) {
                            if (err) {
                                errorback(err);
                            } else {
                                updateUserTieLoop(i + 1, bets, bets[i + 1], tieRatio, res, callback, errorback);
                            }
                        });
                    }
                });
            } else {
                currentBet.profit = -currentBet.coins;
                currentBet.save(function (err) {
                    if (err) {
                        errorback(err);
                    } else {
                        updateUserTieLoop(i + 1, bets, bets[i + 1], tieRatio, res, callback, errorback);
                    }
                });
            }
        });
    } else {
        callback();
    }
}

function updateUserTie(i, bets, currentBet, tieRatio, res) {
    return function (callback, errorback) {
        updateUserTieLoop(i, bets, currentBet, tieRatio, res, callback, errorback);
    }
}

function updateBetsOnWin(res, game, winnerId) {
    return function (callback, errorback) {
        betObject.Bet.findByGameId(res, game._id)(function (bets) {
            var i = 0;
            if (!bets[i]) {
                callback();
                return;
            }

            updateUserWin(i,
                bets,
                bets[i],
                new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game),
                new ratioCalculator.RatioCalculator().getWinTeamBRatio(bets, game),
                winnerId,
                bets[i].game.teamA._id,
                bets[i].game.teamB._id,
                res)(
                function (callbackUserWin) {
                    callback();
                }, function (error) {
                    errorback(error);
                });
        });
    }
}

function updateUserWin(i, resolvedBets, currentBet, winTeamARatio, winTeamBRatio, winnerId, teamAID, teamBID, res) {
    return function (callback, errorback) {
        updateUserWinLoop(i, resolvedBets, res, currentBet, errorback, winnerId, teamAID, winTeamARatio, winTeamBRatio, teamBID, callback);
    }
}

function updateUserWinLoop(i, resolvedBets, res, currentBet, errorback, winnerId, teamAID, winTeamARatio, winTeamBRatio, teamBID, callback) {
    if (i < resolvedBets.length) {
        userObject.User.findById(res, currentBet.user)(function (user) {
            if (currentBet.winner && currentBet.winner._id.toString() === winnerId) {
                if (currentBet.winner._id.toString() === teamAID.toString()) {
                    var profit = Math.ceil(currentBet.coins * winTeamARatio);
                    user.coins += profit;
                    user.save(function (err) {
                        if (err) {
                            errorback(err);
                        } else {
                            currentBet.profit = profit - currentBet.coins;
                            currentBet.save(function (err) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    updateUserWinLoop(i + 1, resolvedBets, res, resolvedBets[i + 1], errorback, winnerId, teamAID, winTeamARatio, winTeamBRatio, teamBID, callback);
                                }
                            });
                        }
                    });
                } else if (currentBet.winner._id.toString() === teamBID.toString()) {
                    var profit = Math.ceil(currentBet.coins * winTeamBRatio);
                    user.coins += profit;
                    user.save(function (err) {
                        if (err) {
                            errorback(err);
                        } else {
                            currentBet.profit = profit - currentBet.coins;
                            currentBet.save(function (err) {
                                if (err) {
                                    errorback(err);
                                } else {
                                    updateUserWinLoop(i + 1, resolvedBets, res, resolvedBets[i + 1], errorback, winnerId, teamAID, winTeamARatio, winTeamBRatio, teamBID, callback);
                                }
                            });
                        }
                    });
                }
            } else {
                currentBet.profit = -currentBet.coins;
                currentBet.save(function (err) {
                    if (err) {
                        errorback(err);
                    } else {
                        updateUserWinLoop(i + 1, resolvedBets, res, resolvedBets[i + 1], errorback, winnerId, teamAID, winTeamARatio, winTeamBRatio, teamBID, callback);
                    }
                });
            }
        });
    } else {
        callback();
    }
}

function tieGame(res, gameId) {
    return function (callback, errorback) {
        gameObject.Game.findById(res, gameId)(function (game) {
            game.tie = true;
            game.save(function (err) {
                if (err) {
                    errorback(err);
                } else {
                    updateBetsOnTie(res, game)(
                        function (callbackBetsOnTie) {
                            callback();
                        },
                        function (error) {
                            errorback(error);
                        }
                    );
                }
            });
        });
    }
}

function setGameWinner(res, game, winnerId) {
    return function (callback, errorback) {
        game.winner = winnerId;
        game.save(function (err) {
            if (err) {
                errorback(err);
            } else {
                updateBetsOnWin(res, game, winnerId)(
                    function (callbackBetsOnWin) {
                        callback();
                    },
                    function (error) {
                        errorback(error);
                    }
                );
            }
        });
    }
}

module.exports = router;