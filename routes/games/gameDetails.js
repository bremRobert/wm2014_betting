var express = require('express');
var router = express.Router();
var moment = require('moment');
var momentTz = require('moment-timezone');

var ratioCalculator = require('../../controller/RatioCalculator.js');
var userObject = require('../../daos/User.js');
var gameObject = require('../../daos/Game.js');
var betObject = require('../../daos/Bet.js');

router.get('/gameDetails/:id', function (req, res) {
    displayGameDetails(res, req);
});

router.post('/gameDetails/:id', function (req, res) {
    var game = req.params.id;
    var userMail = req.session.user;
    var tie = req.param('tie');
    var winner = req.param('winner');
    var coins = req.param('coins');

    if (tie === 'true') {
        winner = undefined;
    }

    userObject.User.findByEmail(res, userMail)(function (user) {
        if (parseInt(coins) > user.coins) {
            displayGameDetails(res, req, 'Sie haben zuwenig Punkte.');
            return;
        }

        var newBet = new res.factory.Bet({
                game: game,
                user: user._id,
                tie: tie == 'true',
                winner: winner,
                coins: parseInt(coins),
                date: Date.now()
            }
        );
        newBet.save(function (err) {
            if (err) {
                console.log(err);
            } else {
                user.coins -= parseInt(coins);
                user.save(function (userErr) {
                    if (userErr) {
                        console.log(userErr);
                    } else {
                        displayGameDetails(res, req);
                    }
                })
            }
        });
    });
});

function displayGameDetails(res, req, errorText) {
    gameObject.Game.findById(res, req.params.id)(function (game) {
        betObject.Bet.findByGameId(res, req.params.id)(function (bets) {
            // var ratioCalculator = new ratioCalculator.RatioCalculator();
            var tieRatio = new ratioCalculator.RatioCalculator().getTieRatio(bets, game);
            var winTeamARatio = new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game);
            var winTeamBRatio = new ratioCalculator.RatioCalculator().getWinTeamBRatio(bets, game);

            userObject.User.findByEmail(res, req.session.user)(function (user) {

                if (user) {
                    var userBetsIndex = 0;
                    var userBets = [];
                    for (var index = 0; index < bets.length; index++) {
                        var currentBet = bets[index];
                        if (currentBet.user._id.toString() == user._id.toString()) {
                            userBets[userBetsIndex++] = bets[index];
                        }
                    }
                }
                
                var now = momentTz.tz(Date.now(), "Europe/Zurich");
                res.render('games/gameDetails', { moment: moment, isAdmin: req.session.isAdmin, game: game,
                    tieRatio: tieRatio, winTeamARatio: winTeamARatio, winTeamBRatio: winTeamBRatio,
                    loggedIn: req.session.user, user: user, errors: errorText, now: now,
                    momentTz: momentTz, userBets: userBets});
            })
        });

    });
}

module.exports = router;