var express = require('express');
var router = express.Router();

var teamObject = require('../../daos/Team.js');

router.get('/', function (req, res) {
    displayAllTeams(res, req);
});

router.post('/', function (req, res, next) {
    var name = req.param('name');

    var newTeam = new res.factory.Team({
        country: name
    });

    newTeam.save(function (err) {
        if (err) {
            console.log(err);
        } else {
            displayAllTeams(res, req);
        }
    });
});

function displayAllTeams(res, req) {
    teamObject.Team.getAll(res)(function (teams) {
        res.render('teams/teams', {teams: teams, isAdmin: req.session.isAdmin});
    });
};

router.get('/deleteTeam/:id', function (req, res) {
    if (!req.session.isAdmin) {
        res.render('users/login');
    }

    res.factory.Team.remove({_id: req.params.id}, function (err) {
        if (err) {
            console.log(err);
        } else {
            displayAllTeams(res, req);
        }
    })
});

module.exports = router;