/// <reference path="../typings/mocha/mocha.d.ts" />
/// <reference path="../controller/RatioCalculator.ts"/>
/// <reference path="../daos/Bet.ts"/>
/// <reference path="../daos/Game.ts"/>
/// <reference path="../daos/Team.ts"/>

var assert = require('assert');
var ratioCalculator = require('../controller/RatioCalculator.js');
var betModul = require('../daos/Bet.js');
var gameModul = require('../daos/Game.js');
var teamModul = require('../daos/Team.js');

describe('RatioCalculator', function () {

    var winner:Team.ITeam = new teamModul.Team();
    winner._id = "1";

    var loser:Team.ITeam = new teamModul.Team();
    loser._id = "2";

    var bets:Array<Bet.IBet> = new Array<Bet.IBet>();

    var defaultBet:Bet.IBet = new betModul.Bet();
    defaultBet._id = "1";
    defaultBet.user = "1";
    defaultBet.tie = false;
    defaultBet.winner = winner;
    defaultBet.coins = 55;

    var defaultGame:Game.IGame = new gameModul.Game();
    defaultGame._id = "1";
    defaultGame.teamA = winner;
    defaultGame.teamB = loser;
    defaultGame.initRatioTeamA = 5.0;
    defaultGame.initRatioTeamB = 2.0;
    defaultGame.initRatioTie = 15.5;
    defaultGame.tie = false;
    defaultGame.winner = winner;

    var wrongGame:Game.IGame = defaultGame.clone();
    wrongGame._id = "2";

    describe('getBetsWithRatio', function () {

        it(' should return the initial value for all bets, because there is just one bet with over 50', function () {
            var game:Game.IGame = defaultGame.clone();
            game.initRatioTeamA = 5.0;
            game.initRatioTeamB = 7.7;
            game.initRatioTie = 3.5;
            game.winner = winner;

            var bet:Bet.IBet = defaultBet.clone();
            bet.game = game;
            bet.winner = winner;
            bet.coins = 115;

            bets[0] = bet;

            var tieRatioCalculated:Array<Bet.IBet> = new ratioCalculator.RatioCalculator().getBetsWithRatio(bets, bets);
            assert.equal(tieRatioCalculated[0].ratioTeamA, 5.0);
            assert.equal(tieRatioCalculated[0].ratioTeamB, 7.7);
            assert.equal(tieRatioCalculated[0].ratioTie, 3.5);
        });

        it(' should return the calculated value for teamA and teamB, because there are one bet with over 50 on teamA and one on teamB', function () {
            var game:Game.IGame = defaultGame.clone();
            game.initRatioTeamA = 5.0;
            game.initRatioTeamB = 7.7;
            game.initRatioTie = 3.5;
            game.winner = winner;

            var bet:Bet.IBet = defaultBet.clone();
            bet.game = game;
            bet.winner = winner;
            bet.coins = 54;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2.game = game;
            bet2.winner = loser;
            bet2.coins = 54;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:Array<Bet.IBet> = new ratioCalculator.RatioCalculator().getBetsWithRatio(bets, bets);
            assert.equal(tieRatioCalculated[0].ratioTeamA, 2.0);
            assert.equal(tieRatioCalculated[0].ratioTeamB, 2.0);
            assert.equal(tieRatioCalculated[0].ratioTie, 0.0);
        });

        it(' should return the calculated value for teamA and teamB, because there are one bet with over 50 on teamB and two bets on teamB who are togheter over 50', function () {
            var game:Game.IGame = defaultGame.clone();
            game.initRatioTeamA = 5.0;
            game.initRatioTeamB = 7.7;
            game.initRatioTie = 3.5;
            game.winner = winner;

            var bet:Bet.IBet = defaultBet.clone();
            bet.game = game;
            bet.winner = winner;
            bet.coins = 30;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2.game = game;
            bet2.winner = loser;
            bet2.coins = 70;

            var bet3:Bet.IBet = defaultBet.clone();
            bet3.game = game;
            bet3.winner = winner;
            bet3.coins = 40;

            bets[0] = bet;
            bets[1] = bet2;
            bets[2] = bet3;

            var tieRatioCalculated:Array<Bet.IBet> = new ratioCalculator.RatioCalculator().getBetsWithRatio(bets, bets);
            assert.equal(tieRatioCalculated[0].ratioTeamA, 2.0);
            assert.equal(tieRatioCalculated[0].ratioTeamB, 2.0);
            assert.equal(tieRatioCalculated[0].ratioTie, 0.0);
        });
    });

    describe('getTieRatio', function () {

        it(' should return the initial value if no bet is referencing this game.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.initRatioTie = 3.5;
            var bet:Bet.IBet = defaultBet.clone();
            bet.game = wrongGame;
            bets[0] = bet;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getTieRatio(bets, game);
            assert.equal(tieRatioCalculated, 3.5);
        });

        it(' should return the calculated value because a tie bet and a winner bet are set. Another bet for another game should not be calculated for this ratio.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            var coinsToBet:number = 53;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = winner;

            var bet3:Bet.IBet = defaultBet.clone();
            bet3._id = "3";
            bet3.game = wrongGame;
            bet3.coins = 50;
            bet3.tie = true;

            bets[0] = bet;
            bets[1] = bet2;
            bets[2] = bet3;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getTieRatio(bets, game);
            assert.equal(tieRatioCalculated, 2.);
        });

        it(' should return the initial ratio because the tie bet is lower than the minimum.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.initRatioTie = 13.25;

            var coinsToBet:number = 53;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = 35;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = winner;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getTieRatio(bets, game);
            assert.equal(tieRatioCalculated, 13.25);
        });

        it(' should return the calculated value because a tie bet and a loser bet are set.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            var coinsToBet:number = 54;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getTieRatio(bets, game);
            assert.equal(tieRatioCalculated, 2.);
        });

        it(' should return 1.25 because the bet on tie is 4 times greater than the bet on the loser.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            var coinsToBet:number = 58;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet * 4;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getTieRatio(bets, game);
            assert.equal(tieRatioCalculated, 1.25);
        });

        it(' should return 5 because the bet on loser is 4 times greater than the bet on tie.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            var coinsToBet:number = 56;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet * 4;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getTieRatio(bets, game);
            assert.equal(tieRatioCalculated, 5);
        });
    });

    describe('getWinTeamARatio', function () {

        it(' should return the initial value if no bet is referencing this game.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.initRatioTeamA = 23.5;
            var bet:Bet.IBet = defaultBet.clone();
            bet.game = wrongGame;

            bets = new Array<Bet.IBet>();
            bets[0] = bet;


            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game);
            assert.equal(tieRatioCalculated, 23.5);
        });

        it(' should return the calculated value because a tie bet and a winnerA bet are set.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = winner;
            game.teamB = loser;

            var coinsToBet:number = 50;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = winner;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game);
            assert.equal(tieRatioCalculated, 2.);
        });

        it(' should return the initial ratio because the teamA bet is lower than the minimum.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.initRatioTeamA = 23.5;
            game.teamA = winner;
            game.teamB = loser;

            var coinsToBet:number = 50;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = 23;
            bet2.tie = false;
            bet2.winner = winner;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game);
            assert.equal(tieRatioCalculated, 23.5);
        });

        it(' should return the initial ratio because the teamA bet is lower than the minimum.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = winner;
            game.teamB = loser;
            game.initRatioTeamA = 13.25;

            var coinsToBet:number = 50;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = 42;
            bet2.tie = false;
            bet2.winner = winner;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game);
            assert.equal(tieRatioCalculated, 13.25);
        });

        it(' should return the calculated value because a winnerA bet and a loserB bet are set.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = winner;
            game.teamB = loser;

            var coinsToBet:number = 54;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = false;
            bet.winner = winner;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game);
            assert.equal(tieRatioCalculated, 2.);
        });

        it(' should return 1.25 because the bet on teamA is 4 times greater than the bet on teamB.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = winner;
            game.teamB = loser;
            game.winner = winner;

            var coinsToBet:number = 53;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet * 4;
            bet.tie = false;
            bet.winner = winner;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game);
            assert.equal(tieRatioCalculated, 1.25);
        });

        it(' should return 5 because the bet on teamB is 4 times greater than the bet on winnerTeamA.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = winner;
            game.teamB = loser;
            game.winner = winner;

            var coinsToBet:number = 52;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = false;
            bet.winner = winner;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet * 4;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamARatio(bets, game);
            assert.equal(tieRatioCalculated, 5);
        });
    });

    describe('getWinTeamBRatio', function () {

        it(' should return the initial value if no bet is referencing this game.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.initRatioTeamB = 63.23;
            var bet:Bet.IBet = defaultBet.clone();
            bet.game = wrongGame;

            bets = new Array<Bet.IBet>();
            bets[0] = bet;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamBRatio(bets, game);
            assert.equal(tieRatioCalculated, 63.23);
        });

        it(' should return the calculated value because a tie bet and a winnerB bet are set.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = loser;
            game.teamB = winner;

            var coinsToBet:number = 51;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = winner;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamBRatio(bets, game);
            assert.equal(tieRatioCalculated, 2.);
        });

        it(' should return the initial ratio because the bet on teamB is too low.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = loser;
            game.teamB = winner;
            game.initRatioTeamB = 3.23;

            var coinsToBet:number = 51;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = true;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = 13;
            bet2.tie = false;
            bet2.winner = winner;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamBRatio(bets, game);
            assert.equal(tieRatioCalculated, 3.23);
        });

        it(' should return the calculated value because a winnerB bet and a loserA bet are set.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = loser;
            game.teamB = winner;
            game.winner = winner;

            var coinsToBet:number = 52;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = false;
            bet.winner = winner;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamBRatio(bets, game);
            assert.equal(tieRatioCalculated, 2.);
        });

        it(' should return 1.25 because the bet on teamB is 4 times greater than the bet on teamA.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = loser;
            game.teamB = winner;
            game.winner = winner;

            var coinsToBet:number = 57;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet * 4;
            bet.tie = false;
            bet.winner = winner;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamBRatio(bets, game);
            assert.equal(tieRatioCalculated, 1.25);
        });

        it(' should return 5 because the bet on teamA is 4 times greater than the bet on winnerTeamB.', function () {
            bets = new Array<Bet.IBet>();
            var game:Game.IGame = defaultGame.clone();
            game.teamA = loser;
            game.teamB = winner;
            game.winner = winner;

            var coinsToBet:number = 56;

            var bet:Bet.IBet = defaultBet.clone();
            bet._id = "1";
            bet.game = game;
            bet.coins = coinsToBet;
            bet.tie = false;
            bet.winner = winner;

            var bet2:Bet.IBet = defaultBet.clone();
            bet2._id = "2";
            bet2.game = game;
            bet2.coins = coinsToBet * 4;
            bet2.tie = false;
            bet2.winner = loser;

            bets[0] = bet;
            bets[1] = bet2;

            var tieRatioCalculated:number = new ratioCalculator.RatioCalculator().getWinTeamBRatio(bets, game);
            assert.equal(tieRatioCalculated, 5);
        });
    });
});
