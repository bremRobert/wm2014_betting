var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var routes = require('./routes/index');
var users = require('./routes/users');
var userLoginRoutes = require('./routes/users/login');
var highscores = require('./routes/users/highscores');
var otherUser = require('./routes/users/otherUser');
var games = require('./routes/games');
var closeGames = require('./routes/games/closeGames');
var gameDetails = require('./routes/games/gameDetails');
var teams = require('./routes/teams');
var bets = require('./routes/bets');

var Factory = require('./module.factory');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(favicon());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
    secret: 'keyboard cat'
}));

app.use(function (req, res, next) {
    res.locals.session = req.session;
    next();
});

app.use(function (req, res, next) {
    res.factory = factory;
    next();
});

app.use('/', routes);
app.use('/user', users);
app.use('/user', userLoginRoutes);
app.use('/user', highscores);
app.use('/user', otherUser);
app.use('/games', games);
app.use('/games', closeGames);
app.use('/games', gameDetails);
app.use('/teams', teams);
app.use('/bets', bets);

/// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {

    mongoose.connect('mongodb://localhost/mydatabase');

    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
} else {
    mongoose.connect(process.env.CUSTOMCONNSTR_FOOTBALL_DB);
}

var db = mongoose.connection;
var factory = new Factory(Schema, mongoose);
factory.createSchemas();

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
