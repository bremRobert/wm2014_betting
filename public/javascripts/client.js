window.onload = init;

function init() {
    var game = $('#game');
    var winner = $('#winner');

    game.change(function () {
        winner.load('/bets/getOptionsForGame/' + game.val());
    });
}


var showAllGames = false;
function updateGames() {
    if (showAllGames) {
        $('.gameTable').load('/games/onlyOpenGames');
    } else {
        $('.gameTable').load('/games/allGames');
    }
    showAllGames = !showAllGames;
}

$(document).ready(function () {
    $('#save').click(function (event) {

        data = $('#pass').val();
        var len = data.length;

        if (len < 1) {
            $('#errors').html("Passwort darf nicht leer sein.");
            event.preventDefault();
        }

        if ($('#pass').val() != $('#passConfirm').val()) {
            $('#errors').html("Passwörter müssen übereinstimmen.");
            event.preventDefault();
        }

    });

    $(".inlineTable").on("click", ".clickableRow", function () {
        window.document.location = $(this).attr("href");
    });
});
