/// <reference path="../typings/node/node.d.ts" />
/// <reference path="../daos/Game.ts"/>
/// <reference path="../daos/Bet.ts"/>
var RatioCalculator;
(function (_RatioCalculator) {
    var RatioCalculator = (function () {
        function RatioCalculator() {
            this.tieCoins = 0;
            this.teamACoins = 0;
            this.teamBCoins = 0;
            this.tieRatio = 0;
            this.teamARatio = 0;
            this.teamBRatio = 0;
            this.smallestAllowedCoinsPerPossibility = 50;
        }
        RatioCalculator.prototype.getBetsWithRatio = function (betsToDisplay, allBets) {
            for (var index = 0; index < betsToDisplay.length; index++) {
                if (betsToDisplay[index]) {
                    betsToDisplay[index].ratioTeamA = this.getWinTeamARatio(allBets, betsToDisplay[index].game);
                    betsToDisplay[index].ratioTeamB = this.getWinTeamBRatio(allBets, betsToDisplay[index].game);
                    betsToDisplay[index].ratioTie = this.getTieRatio(allBets, betsToDisplay[index].game);

                    var ratio;
                    if (betsToDisplay[index].tie) {
                        ratio = betsToDisplay[index].ratioTie;
                    } else if (betsToDisplay[index].winner._id.toString() == betsToDisplay[index].game.teamA._id.toString()) {
                        ratio = betsToDisplay[index].ratioTeamA;
                    } else if (betsToDisplay[index].winner._id.toString() == betsToDisplay[index].game.teamB._id.toString()) {
                        ratio = betsToDisplay[index].ratioTeamB;
                    }

                    betsToDisplay[index].previewProfit = (betsToDisplay[index].coins * ratio) - betsToDisplay[index].coins;
                }
            }

            return betsToDisplay;
        };

        RatioCalculator.prototype.getGamesWithRatio = function (games, allBets) {
            for (var index = 0; index < games.length; index++) {
                if (games[index]) {
                    games[index].ratioTeamA = this.getWinTeamARatio(allBets, games[index]);
                    games[index].ratioTeamB = this.getWinTeamBRatio(allBets, games[index]);
                    games[index].ratioTie = this.getTieRatio(allBets, games[index]);
                }
            }

            return games;
        };

        RatioCalculator.prototype.getTieRatio = function (bets, game) {
            if (game.finalRatioTie) {
                return game.finalRatioTie;
            }
            this.calculate(bets, game);
            return this.tieRatio;
        };

        RatioCalculator.prototype.getWinTeamARatio = function (bets, game) {
            if (game.finalRatioTeamA) {
                return game.finalRatioTeamA;
            }
            this.calculate(bets, game);
            return this.teamARatio;
        };

        RatioCalculator.prototype.getWinTeamBRatio = function (bets, game) {
            if (game.finalRatioTeamB) {
                return game.finalRatioTeamB;
            }
            this.calculate(bets, game);
            return this.teamBRatio;
        };

        RatioCalculator.prototype.calculate = function (bets, game) {
            this.tieRatio = 0;
            this.teamARatio = 0;
            this.teamBRatio = 0;
            this.tieCoins = 0;
            this.teamACoins = 0;
            this.teamBCoins = 0;

            this.calculateCoinsPerResult(bets, game);
            var totalCoins = this.tieCoins + this.teamACoins + this.teamBCoins;

            if (this.tieCoins > 0) {
                this.tieRatio = totalCoins / this.tieCoins;
            }
            if (this.teamACoins > 0) {
                this.teamARatio = totalCoins / this.teamACoins;
            }
            if (this.teamBCoins > 0) {
                this.teamBRatio = totalCoins / this.teamBCoins;
            }

            if (this.twoPossibilitiesAreZero() || !this.twoPossibilitiesAreBigEnough()) {
                if (!game) {
                    return;
                }
                this.tieRatio = game.initRatioTie;
                this.teamARatio = game.initRatioTeamA;
                this.teamBRatio = game.initRatioTeamB;
            }

            if (game && game.date && game.date.getTime() < Date.now()) {
                game.finalRatioTeamA = this.teamARatio;
                game.finalRatioTeamB = this.teamBRatio;
                game.finalRatioTie = this.tieRatio;

                game.save();
            }
        };

        RatioCalculator.prototype.twoPossibilitiesAreBigEnough = function () {
            var teamACorrect = this.teamACoins >= this.smallestAllowedCoinsPerPossibility;
            var teamBCorrect = this.teamBCoins >= this.smallestAllowedCoinsPerPossibility;
            var tieCorrect = this.tieCoins >= this.smallestAllowedCoinsPerPossibility;

            return teamACorrect && teamBCorrect || teamACorrect && tieCorrect || teamBCorrect && tieCorrect;
        };

        RatioCalculator.prototype.twoPossibilitiesAreZero = function () {
            return this.tieRatio === 0 && this.teamARatio === 0 || this.tieRatio === 0 && this.teamBRatio === 0 || this.teamARatio === 0 && this.teamBRatio === 0;
        };

        RatioCalculator.prototype.calculateCoinsPerResult = function (bets, game) {
            if (!bets || !game) {
                return;
            }

            for (var i = 0; i < bets.length; i++) {
                var currentBet = bets[i];

                if (!currentBet || !currentBet.game || !currentBet.game._id || !game._id || currentBet.game._id.toString() != game._id.toString()) {
                    continue;
                }

                if (currentBet.tie) {
                    this.tieCoins += currentBet.coins;
                } else {
                    if (!game || !game.teamA || !game.teamA._id || !game.teamB || !game.teamB._id || !currentBet.winner || !currentBet.winner._id) {
                        continue;
                    }

                    if (game.teamA._id.toString() == currentBet.winner._id.toString()) {
                        this.teamACoins += currentBet.coins;
                    } else if (game.teamB._id.toString() == currentBet.winner._id.toString()) {
                        this.teamBCoins += currentBet.coins;
                    }
                }
            }
        };
        return RatioCalculator;
    })();
    _RatioCalculator.RatioCalculator = RatioCalculator;
})(RatioCalculator || (RatioCalculator = {}));

exports.RatioCalculator = RatioCalculator.RatioCalculator;
//# sourceMappingURL=RatioCalculator.js.map
