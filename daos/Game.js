/// <reference path="../typings/node/node.d.ts" />
/// <reference path="../typings/mongoose/mongoose.d.ts"/>
/// <reference path="Team.ts"/>
/// <reference path="Cloneable.ts" />
/// <reference path="ObjectHelper.ts" />
var objectHelper = require('./ObjectHelper.js');

var Game;
(function (_Game) {
    var Game = (function () {
        function Game() {
        }
        Game.prototype.save = function () {
            console.log("save game");
        };

        Game.findById = function (res, id) {
            return function (callback, errorback) {
                res.factory.Game.find({ _id: id }).populate('teamA teamB winner').exec(function (err, games) {
                    if (err) {
                        errorback(err);
                    } else {
                        callback(games[0]);
                    }
                });
            };
        };

        Game.getAll = function (res) {
            return function (callback, errorback) {
                res.factory.Game.find({}).populate('teamA teamB winner').sort('-date').exec(function (err, games) {
                    if (err) {
                        errorback(err);
                    } else {
                        callback(games);
                    }
                });
            };
        };

        Game.findByUserId = function (res, userId) {
            return function (callback, errorback) {
                res.factory.Game.find({ _id: userId }).populate('teamA teamB').exec(function (err, games) {
                    if (err) {
                        errorback(err);
                    } else {
                        callback(games[0]);
                    }
                });
            };
        };

        Game.prototype.clone = function () {
            return new objectHelper.ObjectHelper().clone(this);
        };

        Game.prototype.equals = function (other) {
            return this._id.toString() === other._id.toString();
        };
        return Game;
    })();
    _Game.Game = Game;
})(Game || (Game = {}));

exports.Game = Game.Game;
//# sourceMappingURL=Game.js.map
