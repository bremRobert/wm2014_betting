/// <reference path="../typings/node/node.d.ts" />
var ObjectHelper;
(function (_ObjectHelper) {
    var ObjectHelper = (function () {
        function ObjectHelper() {
        }
        ObjectHelper.prototype.augment = function (obj, properties) {
            for (var key in properties) {
                obj[key] = properties[key];
            }
        };

        ObjectHelper.prototype.clone = function (obj) {
            var retval = {};
            this.augment(retval, obj);
            return retval;
        };
        return ObjectHelper;
    })();
    _ObjectHelper.ObjectHelper = ObjectHelper;
})(ObjectHelper || (ObjectHelper = {}));

exports.ObjectHelper = ObjectHelper.ObjectHelper;
//# sourceMappingURL=ObjectHelper.js.map
