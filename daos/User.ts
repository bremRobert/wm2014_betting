/// <reference path="../typings/mongoose/mongoose.d.ts"/>
/// <reference path="../typings/node/node.d.ts" />
/// <reference path="Cloneable.ts" />
/// <reference path="ObjectHelper.ts" />

var objectHelper = require('../daos/ObjectHelper.js');

module User {

    export interface IUser extends Cloneable.Cloneable<IUser> {
        _id: string;
        email: string;
        name: string;
        password: string;
        coins: number;
        isAdmin: boolean;
        created:Date;
    }

    export class User implements IUser {
        _id:string;
        email:string;
        name:string;
        password:string;
        coins:number;
        isAdmin:boolean;
        created:Date;

        constructor() {
            this.created = new Date(Date.now());
        }

        public static getAll(res:any):any {
            return function (callback, errorback) {
                res.factory.User.find({})
                    .exec(function (error, users) {
                        if (error) {
                            errorback(error);
                        } else {
                            callback(users);
                        }
                    });
            }
        }

        public static findById(res:any, id:string):any {
            return function (callback, errorback) {
                res.factory.User.find({_id: id}, function (userError, userResult) {
                    if (userError) {
                        errorback(userError);
                    } else {
                        callback(userResult[0]);
                    }
                });
            }
        }

        public static findByEmail(res:any, email:string):any {
            return function (callback, errorback) {
                res.factory.User.find({ email: email }, function (error, users) {
                        if (error) {
                            errorback(error);
                        } else {
                            callback(users[0]);
                        }
                    }
                );
            }
        }

        public clone():User {
            return <User> new objectHelper.ObjectHelper().clone(this);
        }

        public equals(other:IUser):boolean {
            return this._id.toString() === other._id.toString();
        }
    }
}

exports.User = User.User;