/// <reference path="../typings/mongoose/mongoose.d.ts"/>
/// <reference path="../typings/node/node.d.ts" />
/// <reference path="Game.ts" />
/// <reference path="Team.ts" />
/// <reference path="Cloneable.ts" />
/// <reference path="ObjectHelper.ts" />
/// <reference path="../controller/RatioCalculator.ts" />

var objectHelper = require('../daos/ObjectHelper.js');
var gameObject = require('../daos/Game.js');
var ratioCalculator = require('../controller/RatioCalculator.js');

module Bet {

    export interface IBetMongoose extends mongoose.Document, IBet {

    }

    export interface IBet extends Cloneable.Cloneable<IBet> {
        _id:string;
        game:Game.IGame;
        user:string;
        tie:Boolean;
        winner:Team.ITeam;
        date:Date;
        coins:number;
        profit:number;

        // Not in DB
        ratioTeamA:number;
        ratioTeamB:number;
        ratioTie:number;
        previewProfit:number;
    }

    export class Bet implements IBet {
        public _id:string;
        public game:Game.IGame;
        public user:string;
        public tie:Boolean;
        public winner:Team.ITeam;
        public date:Date;
        public coins:number;
        public profit:number;

        // Not in DB
        public ratioTeamA:number;
        public ratioTeamB:number;
        public ratioTie:number;
        public previewProfit:number;

        public static pageSize:number = 15;
        private resolvedBetsByUser:Array<IBet>;

        constructor() {
        }

        private setResolvedBetsByUser = (resolvedBetsByUser:Array<IBet>):void => {
            this.resolvedBetsByUser = resolvedBetsByUser;
        }

        public getPageSize = ():number => {
            return Math.ceil(this.resolvedBetsByUser.length / Bet.pageSize);
        }

        public getResolvedBetsByUserPage = (page:number, forceReload:boolean, res:any, userId:string):any => {
            var setResolvedBetsByUser = this.setResolvedBetsByUser;
            return (callback, errorback) => {
                this.loadResolvedBetsByUser(res, userId, forceReload)(function (bets) {
                    setResolvedBetsByUser(bets);
                    var pageBets:Array<IBet> = new Array<IBet>();
                    var startIndex:number = (page - 1) * Bet.pageSize;
                    for (var index:number = startIndex; index < bets.length && index < page * Bet.pageSize; index++) {
                        pageBets[index - startIndex] = bets[index];
                    }
                    callback(pageBets);
                });
            }
        }

        private loadResolvedBetsByUser = (res:any, userId:string, forceReload:boolean):any => {
            return (callback, errorback) => {
                if (forceReload || this.resolvedBetsByUser === undefined) {
                    gameObject.Game.getAll(res)(function (games) {
                        Bet.findByUserId(res, userId)(function (betsToDisplay) {
                            Bet.getAll(res)(function (allBets) {
                                callback(new ratioCalculator.RatioCalculator().getBetsWithRatio(betsToDisplay, allBets));
                            });
                        });
                    });
                } else {
                    callback(this.resolvedBetsByUser);
                }
            }
        }

        public static findByGameId(res:any, gameId:string) {
            return function (callback, errorback) {
                res.factory.Bet.find({game: gameId})
                    .populate('game user winner')
                    .exec(function (error, bets) {
                        if (error) {
                            errorback(error);
                        } else {
                            var options = {
                                path: 'game.teamA game.teamB',
                                model: 'Team'
                            };

                            res.factory.Bet.populate(bets, options, function (errorBet, resolvedBets) {
                                if (errorBet) {
                                    errorback(errorBet);
                                } else {
                                    callback(resolvedBets);
                                }
                            });
                        }
                    });
            }
        }

        public static getById(res:any, id:string):any {
            return function (callback, errorback) {
                res.factory.Bet.find({_id: id})
                    .populate('game')
                    .exec(function (betErr, bets) {
                        if (betErr) {
                            errorback(betErr);
                        } else {
                            callback(bets[0]);
                        }
                    });
            }
        }

        public static findByUserId(res:any, userId:string):any {
            return function (callback, errorback) {
                res.factory.Bet.find({user: userId})
                    .populate('game user winner')
                    .sort('-date')
                    .exec(function (err, bets) {
                        if (err) {
                            errorback(err);
                        } else {
                            var options = {
                                path: 'game.teamA game.teamB',
                                model: 'Team'
                            };

                            res.factory.Bet.populate(bets, options, function (error, resolved) {
                                if (error) {
                                    errorback(error);
                                } else {
                                    callback(resolved);
                                }
                            });
                        }
                    });
            }
        }

        public static getAll(res:any):any {
            return function (callback, errorback) {
                res.factory.Bet.find({})
                    .populate('game user winner')
                    .sort('-date')
                    .exec(function (err, bets) {
                        if (err) {
                            errorback(err);
                        } else {
                            var options = {
                                path: 'game.teamA game.teamB',
                                model: 'Team'
                            };

                            res.factory.Bet.populate(bets, options, function (error, resolved) {
                                if (error) {
                                    errorback(error);
                                } else {
                                    callback(resolved);
                                }
                            });
                        }
                    });
            }
        }

        public clone():Bet {
            return <Bet> new objectHelper.ObjectHelper().clone(this);
        }
    }
}

exports.Bet = Bet.Bet;