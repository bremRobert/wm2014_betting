/// <reference path="../typings/mongoose/mongoose.d.ts"/>
/// <reference path="../typings/node/node.d.ts" />
/// <reference path="Cloneable.ts" />
/// <reference path="ObjectHelper.ts" />
var objectHelper = require('../daos/ObjectHelper.js');

var Team;
(function (_Team) {
    var Team = (function () {
        function Team() {
        }
        Team.getAll = function (res) {
            return function (callback, errorback) {
                res.factory.Team.find({}, function (error, teams) {
                    if (error) {
                        errorback(error);
                    } else {
                        callback(teams);
                    }
                });
            };
        };

        Team.prototype.clone = function () {
            return new objectHelper.ObjectHelper().clone(this);
        };

        Team.prototype.equals = function (other) {
            return this._id.toString() === other._id.toString();
        };
        return Team;
    })();
    _Team.Team = Team;
})(Team || (Team = {}));

exports.Team = Team.Team;
//# sourceMappingURL=Team.js.map
