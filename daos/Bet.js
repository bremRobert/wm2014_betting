/// <reference path="../typings/mongoose/mongoose.d.ts"/>
/// <reference path="../typings/node/node.d.ts" />
/// <reference path="Game.ts" />
/// <reference path="Team.ts" />
/// <reference path="Cloneable.ts" />
/// <reference path="ObjectHelper.ts" />
/// <reference path="../controller/RatioCalculator.ts" />
var objectHelper = require('../daos/ObjectHelper.js');
var gameObject = require('../daos/Game.js');
var ratioCalculator = require('../controller/RatioCalculator.js');

var Bet;
(function (_Bet) {
    var Bet = (function () {
        function Bet() {
            var _this = this;
            this.setResolvedBetsByUser = function (resolvedBetsByUser) {
                _this.resolvedBetsByUser = resolvedBetsByUser;
            };
            this.getPageSize = function () {
                return Math.ceil(_this.resolvedBetsByUser.length / Bet.pageSize);
            };
            this.getResolvedBetsByUserPage = function (page, forceReload, res, userId) {
                var setResolvedBetsByUser = _this.setResolvedBetsByUser;
                return function (callback, errorback) {
                    _this.loadResolvedBetsByUser(res, userId, forceReload)(function (bets) {
                        setResolvedBetsByUser(bets);
                        var pageBets = new Array();
                        var startIndex = (page - 1) * Bet.pageSize;
                        for (var index = startIndex; index < bets.length && index < page * Bet.pageSize; index++) {
                            pageBets[index - startIndex] = bets[index];
                        }
                        callback(pageBets);
                    });
                };
            };
            this.loadResolvedBetsByUser = function (res, userId, forceReload) {
                return function (callback, errorback) {
                    if (forceReload || _this.resolvedBetsByUser === undefined) {
                        gameObject.Game.getAll(res)(function (games) {
                            Bet.findByUserId(res, userId)(function (betsToDisplay) {
                                Bet.getAll(res)(function (allBets) {
                                    callback(new ratioCalculator.RatioCalculator().getBetsWithRatio(betsToDisplay, allBets));
                                });
                            });
                        });
                    } else {
                        callback(_this.resolvedBetsByUser);
                    }
                };
            };
        }
        Bet.findByGameId = function (res, gameId) {
            return function (callback, errorback) {
                res.factory.Bet.find({ game: gameId }).populate('game user winner').exec(function (error, bets) {
                    if (error) {
                        errorback(error);
                    } else {
                        var options = {
                            path: 'game.teamA game.teamB',
                            model: 'Team'
                        };

                        res.factory.Bet.populate(bets, options, function (errorBet, resolvedBets) {
                            if (errorBet) {
                                errorback(errorBet);
                            } else {
                                callback(resolvedBets);
                            }
                        });
                    }
                });
            };
        };

        Bet.getById = function (res, id) {
            return function (callback, errorback) {
                res.factory.Bet.find({ _id: id }).populate('game').exec(function (betErr, bets) {
                    if (betErr) {
                        errorback(betErr);
                    } else {
                        callback(bets[0]);
                    }
                });
            };
        };

        Bet.findByUserId = function (res, userId) {
            return function (callback, errorback) {
                res.factory.Bet.find({ user: userId }).populate('game user winner').sort('-date').exec(function (err, bets) {
                    if (err) {
                        errorback(err);
                    } else {
                        var options = {
                            path: 'game.teamA game.teamB',
                            model: 'Team'
                        };

                        res.factory.Bet.populate(bets, options, function (error, resolved) {
                            if (error) {
                                errorback(error);
                            } else {
                                callback(resolved);
                            }
                        });
                    }
                });
            };
        };

        Bet.getAll = function (res) {
            return function (callback, errorback) {
                res.factory.Bet.find({}).populate('game user winner').sort('-date').exec(function (err, bets) {
                    if (err) {
                        errorback(err);
                    } else {
                        var options = {
                            path: 'game.teamA game.teamB',
                            model: 'Team'
                        };

                        res.factory.Bet.populate(bets, options, function (error, resolved) {
                            if (error) {
                                errorback(error);
                            } else {
                                callback(resolved);
                            }
                        });
                    }
                });
            };
        };

        Bet.prototype.clone = function () {
            return new objectHelper.ObjectHelper().clone(this);
        };
        Bet.pageSize = 15;
        return Bet;
    })();
    _Bet.Bet = Bet;
})(Bet || (Bet = {}));

exports.Bet = Bet.Bet;
//# sourceMappingURL=Bet.js.map
