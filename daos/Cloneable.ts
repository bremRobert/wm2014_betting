module Cloneable {
    export interface Cloneable<T> {
        clone():T
    }
}