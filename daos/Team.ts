/// <reference path="../typings/mongoose/mongoose.d.ts"/>
/// <reference path="../typings/node/node.d.ts" />
/// <reference path="Cloneable.ts" />
/// <reference path="ObjectHelper.ts" />

var objectHelper = require('../daos/ObjectHelper.js');

module Team {

    export interface ITeam extends Cloneable.Cloneable<ITeam> {
        _id: string;
        country: string;

        equals(other:ITeam):boolean;
    }

    export class Team implements ITeam {
        _id:string;
        country:string;

        constructor() {
        }

        public static getAll(res:any):any {
            return function (callback, errorback) {
                res.factory.Team.find({}, function (error, teams) {
                    if (error) {
                        errorback(error);
                    } else {
                        callback(teams);
                    }
                })
            }
        }

        public clone():Team {
            return <Team> new objectHelper.ObjectHelper().clone(this);
        }

        public equals(other:ITeam):boolean {
            return this._id.toString() === other._id.toString();
        }
    }
}

exports.Team = Team.Team;