/// <reference path="../typings/mongoose/mongoose.d.ts"/>
/// <reference path="../typings/node/node.d.ts" />
/// <reference path="Cloneable.ts" />
/// <reference path="ObjectHelper.ts" />
var objectHelper = require('../daos/ObjectHelper.js');

var User;
(function (_User) {
    var User = (function () {
        function User() {
            this.created = new Date(Date.now());
        }
        User.getAll = function (res) {
            return function (callback, errorback) {
                res.factory.User.find({}).exec(function (error, users) {
                    if (error) {
                        errorback(error);
                    } else {
                        callback(users);
                    }
                });
            };
        };

        User.findById = function (res, id) {
            return function (callback, errorback) {
                res.factory.User.find({ _id: id }, function (userError, userResult) {
                    if (userError) {
                        errorback(userError);
                    } else {
                        callback(userResult[0]);
                    }
                });
            };
        };

        User.findByEmail = function (res, email) {
            return function (callback, errorback) {
                res.factory.User.find({ email: email }, function (error, users) {
                    if (error) {
                        errorback(error);
                    } else {
                        callback(users[0]);
                    }
                });
            };
        };

        User.prototype.clone = function () {
            return new objectHelper.ObjectHelper().clone(this);
        };

        User.prototype.equals = function (other) {
            return this._id.toString() === other._id.toString();
        };
        return User;
    })();
    _User.User = User;
})(User || (User = {}));

exports.User = User.User;
//# sourceMappingURL=User.js.map
