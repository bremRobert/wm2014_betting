/// <reference path="../typings/node/node.d.ts" />
/// <reference path="../typings/mongoose/mongoose.d.ts"/>
/// <reference path="Team.ts"/>
/// <reference path="Cloneable.ts" />
/// <reference path="ObjectHelper.ts" />

var objectHelper = require('./ObjectHelper.js');

module Game {

    export interface IGameMongoose extends mongoose.Document {
    }

    export interface IGame extends Cloneable.Cloneable<IGame> {
        _id: string;
        teamA: Team.ITeam;
        teamB: Team.ITeam;
        initRatioTeamA: number;
        initRatioTeamB: number;
        initRatioTie: number;
        finalRatioTeamA: number;
        finalRatioTeamB: number;
        finalRatioTie: number;
        date: Date;
        tie: Boolean;
        winner: Team.ITeam;

        ratioTeamA:number;
        ratioTeamB:number;
        ratioTie:number;

        save():void;
        equals(other:IGame):boolean;
    }

    export class Game implements IGame {
        public _id:string;
        public teamA:Team.ITeam;
        public teamB:Team.ITeam;
        public initRatioTeamA:number;
        public initRatioTeamB:number;
        public initRatioTie:number;
        public finalRatioTeamA:number;
        public finalRatioTeamB:number;
        public finalRatioTie:number;
        public date:Date;
        public tie:Boolean;
        public winner:Team.ITeam;

        // Not in DB
        public ratioTeamA:number;
        public ratioTeamB:number;
        public ratioTie:number;

        constructor() {
        }

        public save():void {
            console.log("save game");
        }

        public static findById(res:any, id:string):any {
            return function (callback, errorback) {

                res.factory.Game.find({_id: id})
                    .populate('teamA teamB winner')
                    .exec(function (err, games) {
                        if (err) {
                            errorback(err);
                        } else {
                            callback(games[0]);
                        }
                    });
            }
        }

        public static getAll(res:any):any {
            return function (callback, errorback) {
                res.factory.Game.find({})
                    .populate('teamA teamB winner')
                    .sort('-date')
                    .exec(function (err, games) {
                        if (err) {
                            errorback(err);
                        } else {
                            callback(games);
                        }
                    });
            }
        }

        public static findByUserId(res:any, userId:string):any {
            return function (callback, errorback) {
                res.factory.Game.find({_id: userId})
                    .populate('teamA teamB')
                    .exec(function (err, games) {
                        if (err) {
                            errorback(err);
                        } else {
                            callback(games[0]);
                        }
                    });
            }
        }

        public clone():Game {
            return <Game> new objectHelper.ObjectHelper().clone(this);
        }

        public equals(other:IGame):boolean {
            return this._id.toString() === other._id.toString();
        }
    }
}

exports.Game = Game.Game;