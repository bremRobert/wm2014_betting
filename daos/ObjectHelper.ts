/// <reference path="../typings/node/node.d.ts" />

module ObjectHelper {

    export class ObjectHelper {

        public augment(obj, properties) {
            for (var key in properties) {
                obj[key] = properties[key];
            }
        }

        public clone(obj) {
            var retval = {};
            this.augment(retval, obj);
            return retval;
        }
    }
}

exports.ObjectHelper = ObjectHelper.ObjectHelper;